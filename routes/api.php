<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

Route::resource('/v1/catalog', 'APICatalogController', ['only' => ['index', 'show']]);

Route::group(["middleware" => "auth.basic.once"], function () {
  Route::resource('/v1/catalog', 'APICatalogController', ['except' => ['index', 'show', 'create', 'edit']]);
  Route::put('/v1/catalog/{id}/rent', 'APICatalogController@putRent');
  Route::put('/v1/catalog/{id}/return', 'APICatalogController@putReturn');
});
