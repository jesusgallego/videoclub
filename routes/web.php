<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/',  'HomeController@getHome');

Auth::routes();
Route::get('/logout', 'Auth\LoginController@logout');

Route::get('/home', 'HomeController@index');

Route::group(['middleware' => 'auth'], function() {
  Route::get('/catalog', 'CatalogController@getIndex');

  Route::get('/catalog/show/{id}', 'CatalogController@getShow');

  Route::get('/catalog/create', 'CatalogController@getCreate');

  Route::post('/catalog/create', 'CatalogController@postCreate');

  Route::get('/catalog/edit/{id}', 'CatalogController@getEdit');

  Route::put('/catalog/edit/{id}', 'CatalogController@putEdit');

  Route::put('/catalog/rent/{id}', 'CatalogController@putRent');

  Route::put('/catalog/return/{id}', 'CatalogController@putReturn');

  Route::delete('/catalog/delete/{id}', 'CatalogController@deleteMovie');
});
