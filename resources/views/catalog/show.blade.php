@extends('layouts.master')

@section('content')
    <div class="row">
      <div class="col-md-4">
        <img src="{{$pelicula->poster}}" alt="{{$pelicula->title}} poster" class="img-thumbnail">
      </div>
      <div class="col-md-8">
        <table class="table">
          <tr>
            <td>Título</td>
            <td>{{$pelicula->title}}</td>
          </tr>
          <tr>
            <td>Año</td>
            <td>{{$pelicula->year}}</td>
          </tr>
          <tr>
            <td>Director</td>
            <td>{{$pelicula->director}}</td>
          </tr>
          <tr>
            <td>Estado</td>
            <td>{{$pelicula->rented == true ? 'Película actualmente alquilada' : 'Película disponible'}}</td>
          </tr>
          <tr>
            <td>Descripción</td>
            <td>{{$pelicula->synopsis}}</td>
          </tr>
        </table>
      </div>
    </div>
    <div class="row">
      <div class="col-md-offset-4 col-md-8">

        <form action="{{$pelicula->rented ? action('CatalogController@putReturn', $pelicula->id) : action('CatalogController@putRent', $pelicula->id)}}"
          method="POST" style="display:inline">
          {{ method_field('PUT') }}
          {{ csrf_field() }}
          <button type="submit" class="btn {{$pelicula->rented == true ? 'btn-warning' : 'btn-success'}}">
            {{$pelicula->rented == true ? 'Devolver Película' : 'Alquilar Película'}}
          </button>
        </form>

        <a href="{{action('CatalogController@getEdit', [$pelicula->id])}}" class="btn btn-primary"><i class="glyphicon glyphicon-pencil" role="button"></i> Editar Película</a>

        <form action="{{action('CatalogController@deleteMovie', $pelicula->id)}}"
          method="POST" style="display:inline">
          {{ method_field('DELETE') }}
          {{ csrf_field() }}
          <button type="submit" class="btn btn-danger">Borrar Película</button>
        </form>

        <a href="{{action('CatalogController@getIndex')}}" class="btn btn-default"><i class="glyphicon glyphicon-chevron-left" role="button"></i> Volver al catálogo</a>
      </div>
    </div>
@stop
