@extends('layouts.master')

@section('content')

    @foreach ($peliculas as $film)
      <a href="{{url('/catalog/show/'.$film->id)}}">
        <div class="row">
          <div class="col-sm-2">
            <img src="{{$film->poster}}" alt="{{$film->title}} poster" class="img-thumbnail">
          </div>
          <div class="col-sm-10">
            <div class="row">
              <div class="col-sm-12"><h1>{{$film->title}}</h1></div>
            </div>
            <div class="row">
              <div class="col-sm-12">{{$film->year}}</div>
            </div>
          </div>
        </div>
      </a>
    @endforeach

@stop
