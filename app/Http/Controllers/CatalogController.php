<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Movie;
use Notification;

class CatalogController extends Controller
{
    public function getIndex()
    {
      $movies = Movie::all();
      return view('catalog.index', array('peliculas' => $movies));
    }

    public function getShow($id)
    {
      $movie = Movie::findOrFail($id);
      return view('catalog.show', array('pelicula' => $movie));
    }

    public function getCreate()
    {
      return view('catalog.create');
    }

    public function getEdit($id)
    {
      $movie = Movie::findOrFail($id);
      return view('catalog.edit', array('pelicula' => $movie));
    }

    public function postCreate(Request $request)
    {
      $movie = new Movie();
      $movie->title = $request->input('title');
      $movie->year = $request->input('year');
      $movie->poster = $request->input('poster');
      $movie->director = $request->input('director');
      $movie->synopsis = $request->input('synopsis');
      $movie->save();
      Notification::success('La película se ha creado correctamente');
      return redirect()->action('CatalogController@getIndex');
    }

    public function putEdit(Request $request, $id)
    {
      $movie = Movie::findOrFail($id);
      $movie->title = $request->input('title');
      $movie->year = $request->input('year');
      $movie->poster = $request->input('poster');
      $movie->director = $request->input('director');
      $movie->synopsis = $request->input('synopsis');
      $movie->save();
      Notification::success('La película se ha modificado correctamente');
      return redirect()->action('CatalogController@getShow', ['id' => $id]);
    }

    public function putRent($id)
    {
      $movie = Movie::findOrFail($id);
      $movie->rented = true;
      $movie->save();
      Notification::success('La película se ha alquilado correctamente');
      return redirect()->action('CatalogController@getShow', ['id' => $id]);
    }

    public function putReturn($id)
    {
      $movie = Movie::findOrFail($id);
      $movie->rented = false;
      $movie->save();
      Notification::success('La película se ha devuelto correctamente');
      return redirect()->action('CatalogController@getShow', ['id' => $id]);
    }

    public function deleteMovie($id)
    {
      $movie = Movie::findOrFail($id);
      $movie->delete();
      Notification::success('La película se ha eliminado correctamente');
      return redirect()->action('CatalogController@getIndex');
    }
}
